package uva.wscbs;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService //(endpointInterface = "uva.wscbs.Calc")
public class CalcImpl {

	@WebMethod
	public int add(int arg1, int arg2) {
		return (arg1 + arg2);
		
	}

	@WebMethod
	public int sub(int arg1, int arg2) {
		return (arg1 - arg2);
	}

	@WebMethod
	public int mul(int arg1, int arg2) {
		return (arg1 * arg2);
	}

	@WebMethod
	public int div(int arg1, int arg2) {
		return (arg1 / arg2);
	}
	
	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8080/Calc/Calc", new CalcImpl());

	}


}
