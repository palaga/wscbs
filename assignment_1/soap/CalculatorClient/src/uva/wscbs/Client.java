package uva.wscbs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import uva.wscbs.CalcImplService;

public class Client {

	public static void main(String[] args) {
		String[] possibleOps = {"sub","add","div","mul"};
		String chosenOp = "";
		while (chosenOp == "") {
			System.out.println("What operation do you want to perform? (Possible: add,sub,mul,div)");
			BufferedReader ops = new BufferedReader(new InputStreamReader(System.in));
			String receivedOp = "";
			try {
				receivedOp = ops.readLine();
			} catch (IOException e) { }
			
			for(int i = 0; i< possibleOps.length; i++)
			{
				if (possibleOps[i].equals(receivedOp)) 
				{
					chosenOp = receivedOp;
					break;
				}
			}
			if (chosenOp == "") System.out.println("Please enter a valid input.");
		}
		
		int chosenFirst = -1;
		System.out.println("What is the first input?");
		BufferedReader first = new BufferedReader(new InputStreamReader(System.in));
		String receivedFirst = "";
		try {
			receivedFirst = first.readLine();
		} catch (IOException e) { }
		
		chosenFirst = Integer.parseInt(receivedFirst);
		
		int chosenSecond = -1;
		System.out.println("What is the second input?");
		BufferedReader Second = new BufferedReader(new InputStreamReader(System.in));
		String receivedSecond = "";
		try {
			receivedSecond = Second.readLine();
		} catch (IOException e) {
			
		}
		chosenSecond = Integer.parseInt(receivedSecond);
		
		CalcImplService service = new CalcImplService();
		CalcImpl calc = service.getCalcImplPort();
		
		int out;		
		switch(chosenOp)
		{
			case "add": out= calc.add(chosenFirst, chosenSecond); break;
			case "sub": out= calc.sub(chosenFirst, chosenSecond); break;
			case "mul": out= calc.mul(chosenFirst, chosenSecond); break;
			case "div": out= calc.div(chosenFirst, chosenSecond); break;
			default: out=0;
		}
		
		System.out.println("Your result is: "+out);

	}
}
