#! /usr/bin/env python
# vim: set foldmethod=manual :

import requests as r

def try_parse_num (num):
    try: return int(num)
    except ValueError:
        try: return float(num)
        except ValueError: return num


def test_stateless (test, type = "rpn", prot = "http", host = "localhost", port = 8080):
    resp  = r.get(create_url(test[type], prot, host, port))
    value = try_parse_num(resp.text)
    print("%s == %s" % (value, test["result"]))
    return (resp.status_code == test["code"] and value == test["result"])


def create_url (expr = None, prot = "http", host = "localhost", port = 8080, stateless = True):
    return "%s://%s:%s/calc%s%s" % (
        prot,
        host,
        str(port),
        '' if stateless else '2',
        ("/" + expr.replace(" ", "&")) if expr != None else ""
    )


if __name__ == "__main__":
    resp = r.get(create_url("1234.5678 555.222 + 3 *"))
    print(str(resp.status_code) + " " + resp.text)

    SYNTAX_ERROR = "syntax error"
    DIV_BY_0     = "division by zero"

    stateless_tests = [
        {
            "code"   : 200,
            "result" : 2.0,
            "pn"     : "+&1&1",
            "rpn"    : "1&1&+",
            "normal" : "1+1"
        }, {
            "code"   : 400,
            "result" : DIV_BY_0,
            "pn"     : ":&1&0",
            "rpn"    : "1&0&:",
            "normal" : "1:0"
        }, {
            "code"   : 400,
            "result" : SYNTAX_ERROR,
            "pn"     : "+&1&1&1",
            "rpn"    : "1&1&1&+",
            "normal" : "1+1(1)"
        }, {
            "code"   : 400,
            "result" : SYNTAX_ERROR,
            "pn"     : "+",
            "rpn"    : "+",
            "normal" : "+"
        }, {
            "code"   : 400,
            "result" : SYNTAX_ERROR,
            "pn"     : "+&1&1&+&2&2",
            "rpn"    : "1&1&+&2&2&+",
            "normal" : "(1+1)(2+2)"
        }, {
            "code"   : 200,
            "result" : 118.83,
            "pn"     : "+&:&+&-&32.48&44.96&*&71.55&55.86&53.83&44.81",
            "rpn"    : "32.48&44.96&-&71.55&55.86&*&+&53.83&:&44.81&+",
            "normal" : "(((32.48-44.96)+(71.55*55.86)):53.83)+44.81"
        }, {
            "code"   : 200,
            "result" : 20.16,
            "pn"     : ":&+&14.54&*&95.71&:&88.71&13.85&31.13",
            "rpn"    : "14.54&95.71&88.71&13.85&:&*&+&31.13&:",
            "normal" : "(14.54+(95.71*(88.71:13.85))):31.13"
        }, {
            "code"   : 200,
            "result" : 201.48,
            "pn"     : "+&-&91.63&-&:&31.19&77.69&+&45.44&54.57&10.24",
            "rpn"    : "91.63&31.19&77.69&:&45.44&54.57&+&-&-&10.24&+",
            "normal" : "(91.63-((31.19:77.69)-(45.44+54.57)))+10.24"
        }, {
            "code"   : 200,
            "result" : -7172.59,
            "pn"     : "+&14.69&+&*&:&+&51.74&:&15.41&91.98&+&-&61.35&7.54&-&15.04&99.91&*&*&-&25.52&23.58&:&40.87&98.98&:&+&84.69&72.81&+&98.35&28.84&-&17.66&+&*&+&70.49&4.16&95.24&-&93.76&:&13.11&90.59",
            "rpn"    : "14.69&51.74&15.41&91.98&:&+&61.35&7.54&-&15.04&99.91&-&+&:&25.52&23.58&-&40.87&98.98&:&*&84.69&72.81&+&98.35&28.84&+&:&*&*&17.66&70.49&4.16&+&95.24&*&93.76&13.11&90.59&:&-&+&-&+&+",
            "normal" : "14.69+((((51.74+(15.41:91.98)):((61.35-7.54)+(15.04-99.91)))*(((25.52-23.58)*(40.87:98.98))*((84.69+72.81):(98.35+28.84))))+(17.66-(((70.49+4.16)*95.24)+(93.76-(13.11:90.59)))))"
        }, {
            "code"   : 200,
            "result" : 12365815.43,
            "pn"     : "*&+&+&-&:&-&36.09&26.25&36.07&46.54&+&+&58.14&*&18.56&42.02&39.02&71.4&*&-&*&34.62&*&:&17.52&72.3&51.6&59.41&36.7",
            "rpn"    : "36.09&26.25&-&36.07&:&46.54&-&58.14&18.56&42.02&*&+&39.02&+&+&71.4&+&34.62&17.52&72.3&:&51.6&*&*&59.41&-&36.7&*&*",
            "normal" : "(((((36.09-26.25):36.07)-46.54)+((58.14+(18.56*42.02))+39.02))+71.4)*(((34.62*((17.52:72.3)*51.6))-59.41)*36.7)"
        }, {
            "code"   : 200,
            "result" : 2,
            "pn"     : "2",
            "rpn"    : "2",
            "normal" : "2"
    }]

    i = 0
    for test in stateless_tests:
        if test_stateless(test):
            print("success: %s" % test["rpn"])
            i = i + 1
        else:
            print("failed:  %s" % test["rpn"])

    print("%d out of %d tests were successful!" % (i, len(stateless_tests)))
