#! /usr/bin/env python
from flask    import Flask, request
from operator import add, sub, mul, truediv as div
import re


app         = Flask(__name__)
answer      = {}
unique_key  = 0
num_parse   = re.compile("^\d{1,5}$|^\d{0,5}\.\d{1,5}$|^\d{1,5}\.\d{0,5}$")
ops         = {
    "+": add,
    "-": sub,
    "*": mul,
    ":": div
}



###################
# RPN impementation
###################

class RPNSyntaxError (Exception):
    pass


def parse_list (elems):
    for elem in elems:
        try:
            value = float(elem)
            # Should match regex, according to specs.
            if num_parse.match(elem) != None:
                yield value
            else:
                raise RPNSyntaxError
        except ValueError:
            if elem in ops:
                yield ops[elem]
            else:
                raise RPNSyntaxError


def evaluate_rpn (string, float_format = "%.2f", sep = "&"):
    elems = string.strip().split(sep)
    stack = []

    for elem in parse_list(elems):
        # If elem is a number, push it on the stack
        if isinstance(elem, (float, int)):
            stack.append(elem)
        # If it is an operation, pop two elems from
        # the stack and perform the operation.
        else:
            try:
                arg1   = stack.pop()
                arg2   = stack.pop()
                result = elem(arg2, arg1)
                stack.append(result)
            except IndexError:
                raise RPNSyntaxError

    if len(stack) == 1:
        return float_format % stack.pop()
    else:
        raise RPNSyntaxError



##########################
# Stateless implementation
##########################

@app.route("/calc/<expr>")
def calc (expr):
    try:
        value = evaluate_rpn(expr)
        return value, 200
    except RPNSyntaxError:
        return "syntax error", 400
    except ZeroDivisionError:
        return "division by zero", 400



#########################
# Stateful implementation
#########################

@app.route('/calc2',       defaults={'key': None}, methods=['GET', 'PUT', 'POST', 'DELETE'])
@app.route('/calc2/<key>', methods=['GET', 'PUT', 'POST', 'DELETE'])
def calc2 (key):                                 # Dispatch:
    try:
        if key == None:                          # Case: No key given
            return calc2_wo_key()
        elif key in answer:                      # Case: Key given and exists
            return calc2_w_key(key)
        else:                                    # Case: Key given and does not exist
            return "", 404
    except RPNSyntaxError:
        return "syntax error", 400
    except ZeroDivisionError:
        return "division by zero", 400



def calc2_w_key (key, var = "ACC", codec = "ascii"):
    if request.method == 'GET':
        return str(answer[key]), 200             # Return value under given key

    elif request.method == 'PUT':
        ans         = answer[key]                # Get answer under given key
        expr        = get_decoded_data()         # Get the expression
        expr        = expr.replace(var,ans)      # Replace <var> with <answer>
        answer[key] = evaluate_rpn(expr)         # Save the result under given key
        return "", 200

    elif request.method == 'DELETE':
            del answer[key]                      # Try to delete answer under given key
            return "", 204
    else:
        return "Method %s not allowed" % request.method, 405



def calc2_wo_key ():
    global unique_key, answer

    if request.method == 'POST':
        expr                = get_decoded_data() # Get the expression
        session_key         = str(unique_key)    # Store session key
        unique_key         += 1                  # Keep the current key unique
        answer[session_key] = evaluate_rpn(expr) # Save the result under a unique key
        return session_key, 201                  # Return the session key

    elif request.method == 'GET':
        return ','.join(answer.keys()), 200      # Return a comma separated list of keys

    elif request.method == 'DELETE':
        answer = {}                              # Reset answers
        return "", 204

    else:
        return "Method %s not allowed" % request.method, 405


def get_decoded_data (codec = "ascii"):
    return request.form["equation"]



if __name__ == "__main__":
    app.debug = True
    app.run(port=8080)
