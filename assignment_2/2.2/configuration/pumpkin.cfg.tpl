[pumpkin]
group = SOA04
load_dir = ~/pmk-seeds/
rx_dir = rx
working_dir = TEMP

[supernodes]
hosts = 127.0.0.1

[broadcast]
port = 7700
enable = True
supernode = True

[ftp]
port = 7650
enable = True

[http]
enable = True

[acknowledgments]
enable = False

[debug]
enabled = True

[rabbitmq]
fallback = True
host = HOSTNAME
user = soa-cloud
pass = VERY-SECRET-PASSWORD

