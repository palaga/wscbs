from fabric.api import *
from fabric.colors import red, green
import fabric.contrib.files as fabfiles
from ConfigParser import SafeConfigParser
import json
import os.path

# Set environment
conf = None
with open("fab.cfg", "r") as f:
    conf = json.load(f)
    for k, v in conf["env"].items():
        if k != "hosts" or env.hosts == []:
            env[k] = v

all     = ["inject", "filter", "collect"]
chown   = lambda x: run("chown pumpkin:pumpkin %s" % x)
pumpkin = lambda x: sudo(x, user="pumpkin")
dirs    = conf["dirs"]
files   = conf["files"]


#############
# Setup phase
#############
@task
@parallel
def setup ():
    """Setup the systems"""
    execute(prepare_seeds_dir)
    execute(run_git)
    execute(install_pika)
    execute(update)
    execute(install_supervisor)
    execute(push_key)


@parallel
@roles(*all)
def prepare_seeds_dir ():
    """Prepare pumpkin installation"""
    pumpkin("mkdir -p %s" % dirs["seeds"])


@parallel
@roles(*all)
def run_git ():
    with cd(dirs["pumpkin"]):
        pumpkin("HOME=/home/pumpkin git config --global user.email 'you@example.com'")
        pumpkin("HOME=/home/pumpkin git config --global user.name soa04")
        pumpkin("HOME=/home/pumpkin git pull -f -q")


@parallel
@roles(*all)
def install_pika ():
    """Install pika"""
    run("pip install pika")


@parallel
@roles(*all)
def install_screen ():
    """Install screen"""
    run("apt-get -y install screen")

@parallel
@roles(*all)
def update():
    """Update system"""
    run("apt-get update")
    #run("apt-get upgrade")

@task
@parallel
@roles(*all)
def install_supervisor ():
    """Install supervisor daemon"""
    run("apt-get -y install supervisor")


@parallel
@roles(*all)
def push_key ():
    """Add our public key to pumpkin"""
    with open(files["ssh_key"], 'r') as f:
        fabfiles.append("%s/.ssh/authorized_keys" % dirs["home"], f.read())


##################
# Deployment phase
##################
@task
@parallel
def deploy ():
    """Deploy the seeds"""
    with hide("commands"):
        execute(push_config)
        execute(deploy_classifier)
        execute(deploy_inject)
        execute(deploy_filter)
        execute(deploy_collect)
        execute(deploy_startup_script)
        execute(update_supervisor)


@task
def prepare_config ():
    """Prepare pumpkin configuration file"""
    c = SafeConfigParser()
    c.read(files["config.tpl"])

    print(green("Configuring supernodes"))
    oldhosts = c.get("supernodes", "hosts").split(',')
    newhosts = sum(conf["env"]["roledefs"].values(), [])
    c.set("supernodes", "hosts", ','.join(newhosts + oldhosts))

    print(green("Configuring rabbitmq"))
    confset = lambda x: c.set("rabbitmq", x, conf["rabbitmq"][x])
    confset("host")
    confset("pass")

    with open(files["config"], 'w') as f:
        c.write(f)


@parallel
@roles(*all)
def push_config ():
    put(files["config"], dirs["pumpkin"])
    chown("%s/%s" % (dirs["pumpkin"], files["config"]))


@roles("filter")
def deploy_classifier ():
    """Deploy the classifier"""
    pumpkin("mkdir -p %s" % dirs["classifier"])
    put(files["classifier"], dirs["classifier"])


@task
@parallel
@roles(*all)
def update_supervisor ():
    """Deploy supervisor configuration"""
    put("supervisor_pumpkin.conf", "/etc/supervisor/conf.d/pumpkin.conf")
    run("supervisorctl reread")
    run("supervisorctl update")


@roles("inject")
def deploy_inject ():
    """Deploy tweet inject"""
    deploy_seed(files["inject"])


@roles("filter")
def deploy_filter ():
    """Deploy tweet filter"""
    deploy_seed(files["filter"])


@roles("collect")
def deploy_collect ():
    """Deploy tweet collector"""
    deploy_seed(files["collect"])


def deploy_seed (seed):
    """Deploy a seed"""
    put(seed, dirs["seeds"])
    chown("%s/%s" % (dirs["seeds"], os.path.basename(seed)))


@parallel
@roles(*all)
def deploy_startup_script ():
    """Deploy startup script"""
    put("startup.sh", dirs["home"])
    chown("%s/%s" % (dirs["home"], "startup.sh"))
    run("chmod +x %s/%s" % (dirs["home"], "startup.sh"))



###############
# Running phase
###############
@task
@parallel
def runner ():
    """Run pumpkin"""
    execute(push_tweets)
    execute(start_nodes)


@task
@roles("inject")
def push_tweets ():
    """Push tweets to injector"""
    pumpkin("mkdir -p %s" % dirs["tweets"])
    put(files["tweets"], dirs["tweets"])


@task
@parallel
@roles(*all)
def start_nodes ():
    """Start pumpkin nodes"""
    run("supervisorctl start pumpkin")


@task
@parallel
@roles(*all)
def stop_nodes ():
    """Stop pumpkin nodes"""
    run("supervisorctl stop pumpkin")


@task
@parallel
@roles(*all)
def get_status ():
    """Request status"""
    run("supervisorctl status pumpkin")


@task
@roles(*all)
def tail (follow=False):
    """Tail output of host"""
    run("supervisorctl tail %s pumpkin stderr" % ("-f" if follow else ""))


@task
@parallel
@roles(*all)
def clean_nodes ():
    """Remove seeds from nodes"""
    run("rm -f %s/*" % dirs["seeds"])


####################
# Post-process phase
####################
@task
@roles("collect")
def collect_timings ():
    """Collect timings"""
    get("%s/timings.txt" % dirs["pumpkin"], "%(path)s")


@task
@roles("collect")
def collect_stats ():
    """Collect statistics"""
    get(files["stats"], "%(path)s")


@task
def plot ():
    """Generate plot"""
    slocal = files["statslocal"]
    local("sort --output=%s %s" % (slocal, slocal))
    local("gnuplot plot.gnu")

