\documentclass[conference]{IEEEtran}

\usepackage{cite}
\usepackage{url}
\usepackage[pdftex]{graphicx}
%\graphicspath{{../pdf/}{../jpeg/}}
%\DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\usepackage{stfloats}
\fnbelowfloat{}

\newcommand{\uni}{Vrije Universiteit\\Universiteit van Amsterdam\\Amsterdam, The Netherlands}
\newcommand{\occi}{Open Cloud Computing Interface}
\newcommand{\class}[1]{\texttt{#1}}


\title{\occi}
\author{\IEEEauthorblockN{Joran de Waaij\\Group 04}
\IEEEauthorblockA{\uni\\
Email: j.de.waaij@student.vu.nl}
\and
\IEEEauthorblockN{Christian Ouwehand\\Group 04}
\IEEEauthorblockA{\uni\\
Email: c.ouwehand@student.vu.nl}}
\date{\today}

\begin{document}
\maketitle


\begin{abstract}
  \occi{} (OCCI) provides cloud customers and providers with an uniform API for
  cloud resource management. OCCI aims at a better interoperability between cloud
  tools and different cloud implementations. It tries to be modular, extensible
  and implementation independent. We will investigate the extent of
  the capabilities of OCCI and the abstraction presented by this API\@. In this
  document we present our findings, based on the OCCI specifications.
\end{abstract}
\IEEEpeerreviewmaketitle{}

\section{Introduction}
  Managing cloud resources can be quite a complicated task. It can be even
  worse, when you decide to switch to a different cloud platform. You might need
  to completely re-engineer your cloud management workflow. Luckily, a few
  standards have emerged in the past years, like CDMI, OVF and OCCI, to make
  ``the cloud'' a more uniform platform.

  We will focus on the model behind OCCI, the capabilities it supports and the
  abstractions it gives to developers. OCCI is currently specified in three
  different documents: the OCCI core model specification~\cite{occi_core}, the
  OCCI infrastructure specification~\cite{occi_inf} and the OCCI HTTP rendering
  specification~\cite{occi_rest}. Together, these documents form the complete
  specification of the OCCI standard.

  In order to keep OCCI modular and extensible, the model and communication
  protocol are strictly separated. The core model describes the basic concepts
  that form the OCCI model, while the HTTP rendering specifies a communication
  protocol for these basic concepts. The infrastructure specification defines a
  standard extension to the core, with some basic and concrete classes, which
  should be available in most, if not all, cloud systems.

  In this document, we will present a summarization of the three specifications
  given earlier. First of all, we need to ask the question: Do we need yet
  another standard? We try to address this question in Section~\ref{standard}.
  In Section~\ref{core} we will discuss the concepts presented by the core
  specification and in Section~\ref{inf} we amend on this with the default
  extensions of the core. In Section~\ref{http} we will discuss how OCCI is
  extended with a HTTP rendering. Finally, we conclude our summarization in
  Section~\ref{conc}.


\section{The Need For a New Open Standard}\label{standard}
  In order to understand the OCCI standard, we need to understand why we need
  such a specification and what kind of problems it tries to solve. In this
  section we try to show the reader the need for a new standard and how it
  developed.

  OCCI was designed, from the beginning, to prevent vendor
  lock-ins.~\cite{cloud_standard} Many protocols are specified and maintained by
  some vendor. This might result in major problems, when it becomes the de
  facto standard. The vendor has a monopoly on the API and might even enforce
  fees for the use of that particular protocol, due to patents. An other problem
  arises when multiple vendors use different protocols for the same kind of
  product. This can lead to obstacles for a customer when he wants to switch to
  a different vendor. OCCI was developed to overcome these issues and present a
  flexible framework for cloud computing.

  \subsection{The development of the protocol}
  The OCCI project started out by analyzing different cloud use cases. The
  requirements for OCCI were deduced from these use cases, to later form the
  OCCI specification.~\cite{occi_usecases} Many cases were considered, such as
  mappings to the current EC2 API and support for graphical interfaces, but only
  a few items were marked with high priority. However, the list of high priority
  items was still quite big and detailed.

  The OCCI working group had only a few key priorities, with interoperability as
  the most important one. They wanted it to be a flexible, easily extensible and
  capable of discovery of certain functionalities. This led to a modular and
  abstract design. The details of the critical items found through their uses
  cases are abstracted away, to present a modular framework, capable of
  expressing these details. The modularity of the standard is reflected by the
  specification. The specification is spread out over three documents, which
  specify the core concepts, basic infrastructure and the rendering of the
  protocol.

  The abstract level of the specification allows a cloud  implementer to tailor
  it to its needs. For instance, the infrastructure document describes some
  basic resources available in a cloud setup, such as a compute and storage
  resources. When a cloud implementer decides to add a GPU node, he might want
  to expose the GPU specifications to his customers. The OCCI specification
  allows this by extending the compute resource class with some GPU specific
  attributes and add some GPU specific action actions. The discoverability
  property of OCCI allows the customer to discover this special GPU resource
  using any OCCI compliant client.

  \subsection{Implementations}
  Currently, there are a few implementations available, most notably rOCCI\@.
  This is an OCCI implementation in Ruby. It implements both an OCCI compliant
  server and client. The server supports Apache and nginx as front-end and has
  support for multiple back-end API, i.e. EC2. Other frameworks are available
  such as erOCCI in erlang and pyOCNI in python.

  There are a few provides which support OCCI, such as OpenStack and BIG Grid.
  Other early implementors of OCCI are some of the cloud platforms such as
  OpenNebula and Eucalyptus. The most important player in cloud computing,
  Amazon, is still missing in this list of providers. Amazon does not have a
  native implementation for OCCI at the time of writing. Although rOCCI has a
  OCCI to AWS API mapping available, it is not officially supported by Amazon.


\section{The OCCI Core Model}\label{core}
  The OCCI core model specification describes the core principles of the OCCI
  API\@. It presents the reader with a handful of abstract concepts, without
  specifying concrete implementation details. In this section we try to give a
  feel for the basics of the core concepts in OCCI\@.

  The core model uses a handful of classes to specify all needed concepts. It
  has classes to specify resources and links between them and classes specify
  the types, extra attributes and actions available for these resources and
  links. Figure~\ref{fig:occi_uml} presents the relation of these concepts in a
  UML diagram.

  \begin{figure}[!b]
    \centering
    \includegraphics[scale=.5]{occi_uml}
    \caption{OCCI core model from~\cite{occi_core}.}
    \label{fig:occi_uml}
  \end{figure}

  The model builds on two abstract classes: \class{Entity} and \class{Category}.
  These base classes form the basis of the model. Subclasses of the
  \class{Entity} class are used to specify entities, such as resources or links,
  which are available to the cloud customers. The exact implementation of these
  entities, by the cloud provider, is hidden behind this abstraction.  The
  subclasses of \class{Category} can assign properties to the entities in
  different ways. For example, the \class{Kind} subclass, which defines the type
  of an entity.

  The \class{Entity} has two subclasses: \class{Resource} and \class{Link}. A
  cloud provider can specify different resources, such as compute or storage
  resources, using this class. These resources can be linked to each other using
  the \class{Link} class. The number of attributes available to these classes
  are very limited and abstract. Therefore, a cloud provider must subclass these
  classes, to provide entities with more attributes, specific to the \emph{type}
  of the entity.


\section{The OCCI Infrastructure}\label{inf}
This section defines how the OCCI core model described in the previous section
can be extended to be used within IaaS\footnote{Infrastructure as a Service}
environments. This comes with some additional resource types, attributes and
actions.

Figure~\ref{fig:occi_infra_uml} describes the base layout in UML format. The
model shows how each type of resource (Network, Compute or Storage) or link
(Networkinterface or Storagelink) should be described. We will now describes
these three types of resources and some of their attributes.

\emph{Compute} resources are nodes that can process information. Usually actual
virtual machines with a specific purpose. It has attributes such as
\emph{occi.compute.mem} for memory size, \emph{occi.compute.cpu} for cpu speed
and \emph{occi.compute.state} for the current state of the machine (e.g.\
running, booting, error).

\emph{Network} resources represent nodes that act as a layer 2 (Datalink layer,
OSI model) network interface. This node contains attributes such as
\emph{occi.network.vlan} for placing the network node in a certain vlan and
\emph{occi.network.state} to show in which state the network node is.

\emph{Storage} object are defined as information recording resources. These
nodes are meant for just this, storing information. It has attributes as
\emph{occi.storage.size} to define the capacity of the storage unit and
\emph{occi.storage.state} to represent its current state.

These three types of resources do not operate on their own, they need a layer to
link them together. This is where the two objects in the Link class come in.

\emph{NetworkInterface} resources act as connection points for the compute
nodes. These objects have attributes as \emph{occi.networkinterface.mac} for the
MAC address assigned to the interface, \emph{occi.networkinterface.address} to
define the IPv4 or IPv6 address of the interface and
\emph{occi.networkinterface.allocation} to determine how the address is
assigned, dynamic for DHCP and static for manually entered.

\emph{StorageLink} resources are used to connect a storage resource to any other
resource. The OCCI implementation handles all low-level operations needed to use
this storage resource. It contains attributes such as
\emph{occi.storagelink.deviceid} to represent the ID defined by the OCCI service
provider and \emph{occi.storagelink.mountpoint} to define where in the guest
operating system the storage device is mounted.

These five objects make up the base of the OCCI infrastructure specification.
The specification is of course extensible as the OCCI implementation is modular
and basically meant to be extended.

\begin{figure}[!b]
  \centering
  \includegraphics[scale=.3]{occi_infra_uml}
  \caption{OCCI Infrastructure model from~\cite{occi_inf}.}
  \label{fig:occi_infra_uml}
\end{figure}


\section{The OCCI HTTP Rendering}\label{http}
  Interactions with the OCCI core model is done through the
  RESTful\footnote{Representational state transfer} OCCI API, serialized over
  the HTTP/1.1 protocol. The format and name-space of the URL used to access the
  OCCI API are freely definable, although some restrictions apply. A URL is a
  direct reference or access point to a resource and must therefore be unique.
  The resources are controlled using common HTTP verbs: GET for retrieving
  information, POST for creating or updating, PUT can also be used for updating
  and DELETE for actions involving removal. Resources are defined in the
  name-space in a hierarchical fashion; if a request by a GET command is on a
  parent, it must return all its children.

\subsection{Semantics of the HTTP return}\label{httpreturn}
  The return of a request to OCCI can include some distinct HTTP headers. These
  are:
  \begin{itemize}
    \item X-OCCI-Attribute
    \item X-OCCI-Location
  \end{itemize}

These attributes are also returned in their own content-type named
\emph{text/occi} and is used in coherence with \emph{text/plain}. These
attributes adhere to the HTTP/1.1
specification\footnote{ftp://www.ietf.org/rfc/rfc2616.txt}. The X-OCCI-Attribute
variable returns information about a requested resource in a format defined in a
name-space set by the service provider and is comma separated if multiple values
are mentioned in a single header field. This can result in the following return
when doing a GET request on a node:

\begin{verbatim}
X-OCCI-Attribute: occi.mem=256, occi.cpu=1
X-OCCI-Locaton: /nodes/44
\end{verbatim}

This return could be interpreted as node 44 in the /nodes/ container has 256MB
of memory and one cpu. As we said before, this is completely definable by the
OCCI service provider; the name-space is completely free to modify.

\subsection{More HTTP behaviors}\label{httpmore}
In this section we will discuss some more general HTTP features and expectations.

\emph{Security} is a feature that is by default not implemented into OCCI\@. The
HTTP requests are not encrypted or secured in any way. The OCCI forum highly
recommends implementing a form of TLS or SSL on the server that OCCI runs on so
that requests can be done over e.g. HTTPS\@. Authentication is recommended to be
based on a HTTP Basic or HTTP Digest mechanism and should be implemented into
the web server. 

\emph{Caching} can be implemented by extending the HTTP headers to support
\emph{cache-control}, \emph{max-age}, \emph{no-cache} and others. This can be
configured within OCCI as the name-space is freely editable by the
administrators of the implementation.

\emph{Return codes} are to be used as defined in the RFC for HTTP/1.1. A
successful request should always be returned using status code 200 (OK). If a
request is made that will return an erroneous return, status code 406 (Not
acceptable) should be used. If it was chosen to implement a caching mechanism
like we mentioned in the previous section, 304 (Not modified) might be
applicable.

\subsection{Asynchronous implementation}\label{httpasync}
The OCCI implementation is by default setup to handle requests synchronously.
When a request is received by the server, the client will wait until the server
has sent the complete response back and the transaction is complete. The
asynchronous definition in the HTTP rendering section of the OCCI specification
is quite interesting and proposes an interesting solution to the, by default,
synchronous nature of HTTP\@. An asynchronous request is handled in the following
steps:

\begin{enumerate}
  \item The client sends a request to the server, for example creating a new
    node.

  \item The server accepts the connection and sends back a temporary redirect
    (HTTP status code 302) to the client, referring to a temporary page.

  \item The client then keeps polling this temporary page, waiting for a change.

  \item If the server is ready setting up the new node, the temporary page is
    changed to return a permanent redirect (HTTP status code 301) and refers to
    the URI of the newly spawned node.

  \item The client polls the temporary page, but gets a permanent redirect to
    the actual node.
\end{enumerate}

  This solution is interesting because it shows an effective implementation of
  basic HTTP status codes and keeps the load on the server down by making the
  temporary page as small as possible. The client in turn doesn't have to maintain
  an active connection waiting for the new node to be created, it can decide
  itself if it has time and resources to poll the temporary page.

\section{Conclusion}\label{conc}
  The Open Cloud Computing Interface specification is a promising one and we can
  certainly see the advantages of having such a standard. Possible
  implementations offer great fallback capabilities and opportunities for
  disaster recovery. If more cloud service providers would provide an OCCI
  implementation, you would only have to change the back-end target to a backup
  provider in case of failure of the primary provider.  OCCI might even provide
  support for collaboration between rivaling cloud providers, maximizing up-time
  and reliability. Although this seems like an illogical step, this has already
  been implemented in solutions like
  MetaCloud\footnote{http://www.metacloud.com/}. Unfortunately we found no
  evidence that this provider uses OCCI as a communication layer.  A major
  disadvantage of OCCI is that they came relatively late (2011) with a concrete
  specification, long after the biggest cloud service provider at this time,
  Amazon EC2, settled. It is therefore unlikely that Amazon will implement this
  into their systems.


\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,references.bib}
\end{document}


